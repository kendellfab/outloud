package us.kendellfabrici.outloud.cells

import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import us.kendellfabrici.outloud.models.Podcast

class PodcastCell: ListCell<Podcast>() {

    @FXML
    lateinit var ivCover: ImageView
    @FXML
    lateinit var lblTitle: Label
    @FXML
    lateinit var hbPane: HBox

    private var loader: FXMLLoader? = null

    override fun updateItem(item: Podcast?, empty: Boolean) {
        super.updateItem(item, empty)

        if(empty || item == null) {
            text = null
            graphic = null
        } else {
            if(loader == null) {
                loader = FXMLLoader(javaClass.getResource("/fxml/podcastcell.fxml"))
                loader!!.setController(this)
                loader!!.load<Parent>()
            }

            if(item.coverURL != null && item.coverURL != "") {
                ivCover.image = Image(item.coverURL)
            } else {
                // <a href="https://www.freepik.com/free-photos-vectors/music">Music vector created by freepik - www.freepik.com</a>
                ivCover.image = Image(javaClass.getResource("/images/mic.png").toString())
            }
            lblTitle.text = item.title

            text = null
            graphic = hbPane
        }
    }
}