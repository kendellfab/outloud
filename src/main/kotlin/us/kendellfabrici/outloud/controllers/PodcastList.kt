package us.kendellfabrici.outloud.controllers

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javax.inject.Inject

import javafx.fxml.FXML
import javafx.scene.control.ListView
import us.kendellfabrici.outloud.Startup
import us.kendellfabrici.outloud.cells.PodcastCell
import us.kendellfabrici.outloud.managers.Data
import us.kendellfabrici.outloud.messages.MessageBus
import us.kendellfabrici.outloud.messages.NewPodcast
import us.kendellfabrici.outloud.models.Podcast

class PodcastList {

    private var podcasts: List<Podcast>? = null
    private var observableList: ObservableList<Podcast>? = null

    lateinit var data: Data
    @FXML
    lateinit var lvPodcasts: ListView<Podcast>

    @Inject
    lateinit var messageBus: MessageBus

    @FXML
    fun initialize() {
        Startup.component.injectPodcastList(this)
        lvPodcasts.setCellFactory {
            PodcastCell()
        }

        lvPodcasts.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            messageBus.passMessage(newValue)
        }

        messageBus.getEvents().subscribe { t ->
            if(t is NewPodcast) {
                observableList?.add(t.podcast)
            } else if (t is Data) {
                data = t
                loadData()
            }
        }
    }

    private fun loadData() {
        podcasts = data.listPodcasts()

        observableList = FXCollections.observableArrayList<Podcast>()
        observableList!!.addAll(podcasts!!)

        lvPodcasts.items = observableList

//        if(observableList!!.size > 0) {
//            messageBus.passMessage(observableList!![0])
//        }
    }
}
