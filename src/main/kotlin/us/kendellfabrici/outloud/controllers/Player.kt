package us.kendellfabrici.outloud.controllers

import io.reactivex.functions.Consumer
import javafx.beans.InvalidationListener
import javafx.beans.Observable
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.control.Slider
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import javafx.scene.media.MediaView
import javafx.scene.web.WebView
import us.kendellfabrici.outloud.Startup
import us.kendellfabrici.outloud.messages.MessageBus
import us.kendellfabrici.outloud.models.Episode
import javax.inject.Inject
import javax.swing.event.ChangeEvent
import javax.swing.event.ChangeListener
import java.util.concurrent.TimeUnit



class Player {

    private lateinit var selectedEpisode: Episode
    private var mediaPlayer: MediaPlayer? = null

    private lateinit var pauseImage: Image
    private lateinit var playImage: Image

    @FXML
    lateinit var slProgress: Slider
    @FXML
    lateinit var playPauseImage: ImageView
    @FXML
    lateinit var lblEpisodeTitle: Label
    @FXML
    lateinit var mvEpisode: MediaView
    @FXML
    lateinit var lblAt: Label
    @FXML
    lateinit var lblTotal: Label
    @FXML
    lateinit var wvDescription: WebView

    @Inject
    lateinit var messageBus: MessageBus

    @FXML
    fun initialize() {
        Startup.component.injectPlayer(this)

        pauseImage = Image(javaClass.getResource("/images/outline_pause_circle_outline_black_24dp.png").toString())
        playImage = Image(javaClass.getResource("/images/outline_play_circle_outline_black_24dp.png").toString())

        wvDescription.isManaged = false

        messageBus.getEvents().subscribe(object: Consumer<Any> {
            override fun accept(t: Any?) {
                if(t is Episode) {
                    selectedEpisode = t
                    startPlayback()
                }
            }
        })
    }

    @FXML
    fun handlePausePlay(event: MouseEvent) {
        mediaPlayer?.let {
            when {
                it.status == MediaPlayer.Status.PLAYING -> {
                    mediaPlayer?.pause()
                }
                it.status == MediaPlayer.Status.PAUSED -> {
                    mediaPlayer?.play()
                }
                it.status == MediaPlayer.Status.READY -> {
                    mediaPlayer?.play()
                }
                else -> println("Status: ${mediaPlayer?.status}")
            }
        }
    }

    private fun startPlayback() {
        val media = Media(selectedEpisode.contentURL)
        if(mediaPlayer != null) {
            mediaPlayer?.stop()
            mediaPlayer = null
        }
        mediaPlayer = MediaPlayer(media)
        mediaPlayer?.isAutoPlay = false

        mediaPlayer?.currentTimeProperty()?.addListener(object: InvalidationListener {
            override fun invalidated(observable: Observable?) {
                slProgress.value = (mediaPlayer!!.currentTime.toMillis() / mediaPlayer!!.totalDuration.toMillis()) * 100
                lblAt.text = formatTime(mediaPlayer!!.currentTime.toMillis())
            }
        })

        mediaPlayer?.statusProperty()?.addListener { observable, oldValue, newValue ->
            when {
                newValue == MediaPlayer.Status.PLAYING -> {
                    playPauseImage.image = pauseImage
                }
                newValue == MediaPlayer.Status.PAUSED -> {
                    playPauseImage.image = playImage
                }
                newValue == MediaPlayer.Status.READY -> {
                    lblAt.text = formatTime(mediaPlayer!!.currentTime.toMillis())
                    lblTotal.text = formatTime(mediaPlayer!!.totalDuration.toMillis())
                }
            }
        }

        mvEpisode.mediaPlayer = mediaPlayer
        lblEpisodeTitle.text = selectedEpisode.title

        wvDescription.engine.setOnError {
            println("WebView Error: $it")
        }

        if(selectedEpisode.description != null) {
            wvDescription.engine.loadContent("<style>body{font-family: sans-serif; font-size: 11px; background-color: #323232; color: #ababab;}</style>" +selectedEpisode.description!!, "text/html")
            wvDescription.isVisible = true
            wvDescription.isManaged = true
        } else {
            wvDescription.isVisible = false
            wvDescription.isManaged = false
        }
    }

    fun formatTime(millis: Double): String {
        var seconds = Math.round(millis / 1000)
        val hours = TimeUnit.SECONDS.toHours(seconds)
        if (hours > 0)
            seconds -= TimeUnit.HOURS.toSeconds(hours)
        val minutes = if (seconds > 0) TimeUnit.SECONDS.toMinutes(seconds) else 0
        if (minutes > 0)
            seconds -= TimeUnit.MINUTES.toSeconds(minutes)
        return if (hours > 0) String.format("%02d:%02d:%02d", hours, minutes, seconds) else String.format("%02d:%02d", minutes, seconds)
    }
}
