package us.kendellfabrici.outloud.controllers

import javafx.application.Platform
import javax.inject.Inject
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Alert
import javafx.scene.control.TextInputDialog
import us.kendellfabrici.outloud.Startup
import us.kendellfabrici.outloud.managers.Data
import us.kendellfabrici.outloud.messages.MessageBus
import us.kendellfabrici.outloud.messages.NewPodcastError
import javafx.stage.FileChooser
import javafx.stage.Stage
import us.kendellfabrici.outloud.managers.Config
import java.util.concurrent.ExecutorService


class Window {

    var data: Data? = null

    @Inject
    lateinit var messageBus: MessageBus
    @Inject
    lateinit var executorService: ExecutorService
    @Inject
    lateinit var config: Config

    lateinit var stage: Stage

    fun initialize() {
        Startup.component.injectWindow(this)
        val lastDB = config.getLastDB()
        if(lastDB != null) {
            data = Data(messageBus, lastDB, executorService)
            messageBus.passMessage(data!!)
        }

        messageBus.getEvents().subscribe { t ->
            if(t is NewPodcastError) {
                val alert = Alert(Alert.AlertType.ERROR)
                alert.title = "Error loading podcast"
                alert.contentText = t.message
                Platform.runLater {
                    alert.show()
                }
            }
        }
    }

    @FXML
    fun handleAddPodcast(actionEvent: ActionEvent) {
        val dialog = TextInputDialog("")
        dialog.title = "Enter Podcast URL"
        dialog.headerText = "Podcast URL"
        val result = dialog.showAndWait()

        result.ifPresent { s -> data?.loadNewPodcast(s) }
    }

    @FXML
    fun handleUpdatePodcasts(actionEvent: ActionEvent) {
        data?.updatePodcasts()
    }

    @FXML
    fun handleNew(actionEvent: ActionEvent) {
        val fileChooser = FileChooser()
        fileChooser.title = "New Podcast Database"
        fileChooser.initialFileName = ".db"
        val file = fileChooser.showSaveDialog(stage)

        if(file != null) {
            config.setLastDB(file)
            data = Data(messageBus, file, executorService)
            messageBus.passMessage(data!!)
        }
    }

    @FXML
    fun handleOpen(actionEvent: ActionEvent) {
        val fileChooser = FileChooser()
        fileChooser.title = "Select Podcast Database"
        val file = fileChooser.showOpenDialog(stage)

        if(file != null) {
            config.setLastDB(file)
            data = Data(messageBus, file, executorService)
            messageBus.passMessage(data!!)
        }
    }
}
