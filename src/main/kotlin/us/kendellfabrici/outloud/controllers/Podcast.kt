package us.kendellfabrici.outloud.controllers

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import us.kendellfabrici.outloud.Startup
import us.kendellfabrici.outloud.managers.Data
import us.kendellfabrici.outloud.messages.MessageBus
import us.kendellfabrici.outloud.models.Episode
import us.kendellfabrici.outloud.models.Podcast
import java.util.concurrent.ExecutorService
import javax.inject.Inject

class Podcast {

    lateinit var selectedPodcast: Podcast

    @FXML
    lateinit var lblTitle: Label
    @FXML
    lateinit var ivCover: ImageView
    @FXML
    lateinit var lblDescription: Label
    @FXML
    lateinit var lvEpisodes: ListView<Episode>

    @Inject
    lateinit var messageBus: MessageBus
    @Inject
    lateinit var executor: ExecutorService

    lateinit var data: Data

    @FXML
    fun initialize() {
         Startup.component.injectPodcast(this)

        lvEpisodes.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            messageBus.passMessage(newValue)
        }

        messageBus.getEvents().subscribe { t ->
            if(t is Podcast) {
                selectedPodcast = t

                lblTitle.text = selectedPodcast.title
                loadEpisodes()
                if(selectedPodcast.coverURL != null && selectedPodcast.coverURL != "") {
                    executor.submit {
                        val img = Image(selectedPodcast.coverURL)
                        Platform.runLater {
                            ivCover.isVisible = true
                            ivCover.image = img
                        }
                    }
                } else {
                    ivCover.image = Image(javaClass.getResource("/images/mic.png").toString())
                }

                if(t.description != null) {
                    lblDescription.text = t.description!!
                    lblDescription.isVisible = true
                } else {
                    lblDescription.isVisible = false
                }
            } else if(t is Data) {
                data = t
            }
        }
    }

    private fun loadEpisodes() {
        executor.submit {
            val episodes = data.listEpisodes(selectedPodcast.id)

            Platform.runLater {
                val observableList = FXCollections.observableArrayList<Episode>()
                observableList.addAll(episodes)

                lvEpisodes.items = observableList
            }
        }
    }
}
