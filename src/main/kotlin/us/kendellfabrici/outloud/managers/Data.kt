package us.kendellfabrici.outloud.managers

import com.rometools.modules.itunes.FeedInformationImpl
import com.rometools.rome.feed.synd.SyndEntry
import com.rometools.rome.io.FeedException
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import javafx.application.Platform
import us.kendellfabrici.outloud.messages.MessageBus
import us.kendellfabrici.outloud.messages.NewPodcast
import us.kendellfabrici.outloud.messages.NewPodcastError

import java.io.IOException
import java.net.URL
import java.sql.SQLException

import us.kendellfabrici.outloud.models.Episode
import us.kendellfabrici.outloud.models.Podcast
import us.kendellfabrici.outloud.repos.EpisodeRepo
import us.kendellfabrici.outloud.repos.EpisodeRepoImpl
import us.kendellfabrici.outloud.repos.PodcastRepo
import us.kendellfabrici.outloud.repos.PodcastRepoImpl
import java.io.File
import java.lang.Exception
import java.lang.IllegalStateException
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class Data(internal var messageBus: MessageBus, file: File, val executor: ExecutorService) {

    private val podcastRepo: PodcastRepo
    private val episodeRepo: EpisodeRepo

    init {
        val url = "jdbc:sqlite:" + file.absolutePath
        val config = HikariConfig()
        config.jdbcUrl = url
        val dataSource = HikariDataSource(config)

        podcastRepo = PodcastRepoImpl(dataSource)
        episodeRepo = EpisodeRepoImpl(dataSource)
    }

    fun loadNewPodcast(url: String) {
        executor.submit {
            try {
                val feed = SyndFeedInput().build(XmlReader(URL(url)))
//            val module = feed.getModule("http://www.itunes.com/dtds/podcast 1.0.dtd")
//            val feedInfo = module as FeedInformationImpl
                val podcast = Podcast()
                podcast.feedURL = url
                podcast.author = feed.author
                podcast.title = feed.title
                podcast.coverURL = feed.image?.url ?: ""
                podcast.description = feed.description

                val podcastID = podcastRepo.savePodcast(podcast)
                if (podcastID != null) {
                    podcast.id = podcastID

                    val entries = feed.entries
                    for (entry in entries) {
                        val episode = inflateEpisode(podcast.id, entry)
                        episodeRepo.saveEpisode(episode)
                    }

                    Platform.runLater {
                        messageBus.passMessage(NewPodcast(podcast))
                    }
                }

            } catch (e: FeedException) {
                e.printStackTrace()
                messageBus.passMessage(NewPodcastError(e.message ?: "Error unknown"))
            } catch (e: IOException) {
                e.printStackTrace()
                messageBus.passMessage(NewPodcastError(e.message ?: "Error unknown"))
            } catch (e: SQLException) {
                e.printStackTrace()
                messageBus.passMessage(NewPodcastError(e.message ?: "Error unknown"))
            } catch (e: IllegalStateException) {
                e.printStackTrace()
                messageBus.passMessage(NewPodcastError(e.message ?: "Error unknown"))
            } catch(e: Exception) {
                e.printStackTrace()
                Platform.runLater {
                    messageBus.passMessage(NewPodcastError(e.message ?: "General exception thrown"))
                }

            }
        }
    }

    fun updatePodcasts() {
        val ps = podcastRepo.listPodcasts()
        for(podcast in ps) {
            executor.submit {
                loadPodcast(podcast)
            }
        }
    }

    private fun loadPodcast(podcast: Podcast) {
        val feed = SyndFeedInput().build(XmlReader(URL(podcast.feedURL)))

        val entries = feed.entries
        for(entry in entries) {
            val episode = inflateEpisode(podcast.id, entry)
            if(!episodeRepo.episodeExists(podcast.id, episode.title!!)) {
                episodeRepo.saveEpisode(episode)
            } else {
                println("Episode exists: ${episode.title}")
            }
        }
    }

    private fun inflateEpisode(podcastId: Int, entry: SyndEntry): Episode {
        val episode = Episode()
        episode.podcastID = podcastId
        episode.title = entry.title
        episode.author = entry.author
        episode.description = entry.description.value
        episode.publishedAt = entry.publishedDate

        val enclosures = entry.enclosures
        if (enclosures.size > 0) {
            episode.contentURL = enclosures[0].url
            episode.length = enclosures[0].length
        }

        return episode
    }

    fun listPodcasts(): List<Podcast> {
        return podcastRepo.listPodcasts()
    }

    fun listEpisodes(podcastId: Int): List<Episode> {
        return episodeRepo.listEpisodes(podcastId)
    }
}
