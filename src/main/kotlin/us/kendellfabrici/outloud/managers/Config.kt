package us.kendellfabrici.outloud.managers

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import us.kendellfabrici.outloud.models.ConfigData
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.lang.Exception

class Config(val appDir: File, val gson: Gson) {

    private var configData: ConfigData
    private val configFile: File = File(appDir, "config.json")

    init {
        if(!configFile.exists()) {
            configData = ConfigData("")
        } else {
            val reader = JsonReader(FileReader(configFile))
            configData = try {
                gson.fromJson(reader, ConfigData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                ConfigData("")
            }
        }
    }

    fun setLastDB(last: File) {
        configData.lastDB = last.absolutePath
        saveConfig()
    }

    fun getLastDB(): File? {
        if(configData.lastDB != "") {
            return File(configData.lastDB)
        }
        return null
    }

    private fun saveConfig() {
        FileWriter(configFile).use {
            gson.toJson(configData, it)
        }
    }

}