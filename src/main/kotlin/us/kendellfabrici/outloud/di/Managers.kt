package us.kendellfabrici.outloud.di

import com.google.gson.Gson
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource

import java.io.File
import java.sql.SQLException

import javax.inject.Singleton
import dagger.Module
import dagger.Provides
import us.kendellfabrici.outloud.managers.Config
import us.kendellfabrici.outloud.managers.Data
import us.kendellfabrici.outloud.messages.MessageBus
import us.kendellfabrici.outloud.repos.EpisodeRepo
import us.kendellfabrici.outloud.repos.EpisodeRepoImpl
import us.kendellfabrici.outloud.repos.PodcastRepo
import us.kendellfabrici.outloud.repos.PodcastRepoImpl
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Module
class Managers {

    @Provides
    @Singleton
    internal fun providesMessageBus(): MessageBus {
        return MessageBus()
    }

    @Provides
    @Singleton
    internal fun providesExecutorService(): ExecutorService {
        return Executors.newFixedThreadPool(5)
    }

    @Provides
    @Singleton
    internal fun providesAppDir(): File {
        val userHome = System.getProperty("user.home")
        val appDir = "outloud"
        val home = File(userHome)
        val app = File(home, appDir)
        if (!app.exists()) {
            app.mkdir()
        }
        return app
    }

    @Provides
    @Singleton
    internal fun providesGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    internal fun providesConfigManager(appDir: File, gson: Gson): Config {
        return Config(appDir, gson)
    }

    @Provides
    @Singleton
    internal fun providesConnectionSource(appDir: File): HikariDataSource {
        val databaseFile = "outloud.db"
        val dbFile = File(appDir, databaseFile)
        val url = "jdbc:sqlite:" + dbFile.absolutePath
        println(url)

        val config = HikariConfig()
        config.jdbcUrl = url
        return HikariDataSource(config)
    }

    @Provides
    @Singleton
    internal fun providesPodcastRepo(dataSource: HikariDataSource): PodcastRepo {
        try {
            return PodcastRepoImpl(dataSource)
        } catch (e: SQLException) {
            throw RuntimeException(e)
        }

    }

    @Provides
    @Singleton
    internal fun providesEpisodeRepo(dataSource: HikariDataSource): EpisodeRepo {
        try {
            return EpisodeRepoImpl(dataSource)
        } catch (e: SQLException) {
            throw RuntimeException(e)
        }

    }

//    @Provides
//    @Singleton
//    internal fun providesData(podcastRepo: PodcastRepo, episodeRepo: EpisodeRepo, messageBus: MessageBus): Data {
//        return Data(podcastRepo, episodeRepo, messageBus)
//    }
}
