package us.kendellfabrici.outloud.di

import javax.inject.Singleton

import us.kendellfabrici.outloud.Startup
import us.kendellfabrici.outloud.controllers.Player
import us.kendellfabrici.outloud.controllers.Podcast
import us.kendellfabrici.outloud.controllers.PodcastList
import us.kendellfabrici.outloud.controllers.Window

@dagger.Component(modules = [Managers::class])
@Singleton
interface Component {
    fun injectStartup(startup: Startup)
    fun injectWindow(window: Window)
    fun injectPodcastList(podcastList: PodcastList)
    fun injectPodcast(podcast: Podcast)
    fun injectPlayer(player: Player)
}
