package us.kendellfabrici.outloud

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import us.kendellfabrici.outloud.controllers.Window
import us.kendellfabrici.outloud.di.Component
import us.kendellfabrici.outloud.di.DaggerComponent
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import javax.inject.Inject

class Startup : Application() {

    @Inject
    lateinit var executor: ExecutorService

    // <a target="_blank" href="https://www.vecteezy.com">Graphics by: Vecteezy!</a>
    @Throws(Exception::class)
    override fun start(primaryStage: Stage) {
        component.injectStartup(this)
        val loader = FXMLLoader(javaClass.getResource("/fxml/window.fxml"))
        val root = loader.load<Parent>()
        val controller = loader.getController<Window>()
        controller.stage = primaryStage

        val scene = Scene(root)
        scene.stylesheets.add(javaClass.getResource("/style/style.css").toExternalForm())
        primaryStage.title = "Out Loud"
        primaryStage.scene = scene
        primaryStage.icons.add(Image(javaClass.getResource("/images/icon.png").toString()))
        primaryStage.show()
    }

    override fun stop() {
        super.stop()
        executor.shutdownNow()
    }

    companion object {

        lateinit var component: Component

        @JvmStatic
        fun main(args: Array<String>) {
            component = DaggerComponent.builder().build()
            Application.launch(Startup::class.java, *args)
        }
    }
}
