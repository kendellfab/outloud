package us.kendellfabrici.outloud.models

import java.util.Date

class Episode {
    var id: Int = 0
    var title: String? = null
    var author: String? = null
    var description: String? = null
    var contentURL: String? = null
    var publishedAt: Date? = null
    var podcastID: Int = 0
    var length: Long = 0
    var completed: Date? = null

    override fun toString(): String {
        return title ?: ""
    }
}
