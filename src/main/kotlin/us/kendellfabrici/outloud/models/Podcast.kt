package us.kendellfabrici.outloud.models

class Podcast {

    var id: Int = 0
    var title: String? = null
    var author: String? = null
    var coverURL: String? = null
    var feedURL: String? = null
    var description: String? = null

    override fun toString(): String {
        return title ?: ""
    }
}
