package us.kendellfabrici.outloud.repos

import com.zaxxer.hikari.HikariDataSource

import java.sql.SQLException

import us.kendellfabrici.outloud.models.Episode
import java.util.*
import kotlin.collections.ArrayList

class EpisodeRepoImpl @Throws(SQLException::class)
constructor(internal var dataSource: HikariDataSource) : EpisodeRepo {
    init {
        init()
    }

    @Throws(SQLException::class)
    private fun init() {
        dataSource.connection.use { connection ->
            connection.prepareStatement("CREATE TABLE IF NOT EXISTS episode(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "title VARCHAR(2048)," +
                    "author VARCHAR(2048)," +
                    "description TEXT," +
                    "contenturl TEXT," +
                    "publishedat INTEGER," +
                    "podcastid INTEGER," +
                    "length INTEGER," +
                    "completed INTEGER);").execute()


            try {
                connection.prepareStatement("CREATE INDEX title_index ON episode (title)").execute()
            } catch (e: SQLException) {
                if(e.message?.contains("index title_index already exists") != true) {
                    throw e
                }
            }

            try {
                connection.prepareStatement("CREATE INDEX podcast_index ON episode (podcastid)").execute()
            } catch (e: SQLException) {
                if(e.message?.contains("index podcast_index already exists") != true) {
                    throw e
                } else {

                }
            }
        }
    }

    @Throws(SQLException::class)
    override fun saveEpisode(episode: Episode) {
        dataSource.connection.use { connection ->
            val statement = connection.prepareStatement("INSERT INTO episode(title, author, description, contenturl, publishedat, podcastid, length) " + "VALUES(?, ?, ?, ?, ?, ?, ?)")

            statement.setString(1, episode.title)
            statement.setString(2, episode.author)
            statement.setString(3, episode.description)
            statement.setString(4, episode.contentURL)
            statement.setLong(5, episode.publishedAt!!.time)
            statement.setInt(6, episode.podcastID)
            statement.setLong(7, episode.length)

            statement.execute()
        }
    }

    override fun listEpisodes(podcastId: Int): List<Episode> {
        dataSource.connection.use { connection ->
            val statement = connection.prepareStatement("SELECT id, title, author, description, contenturl, publishedat, length, completed FROM episode WHERE podcastid = ? ORDER BY publishedat DESC;")
            statement.setInt(1, podcastId)
            statement.execute()

            val rs = statement.resultSet
            val episodes = ArrayList<Episode>()
            while(rs.next()) {
                val episode = Episode()
                episode.id = rs.getInt(1)
                episode.title = rs.getString(2)
                episode.author = rs.getString(3)
                episode.description = rs.getString(4)
                episode.contentURL = rs.getString(5)
                episode.publishedAt = Date(rs.getLong(6))
                episode.podcastID = podcastId
                episode.length = rs.getLong(7)
                episode.completed = Date(rs.getLong(8))

                episodes.add(episode)
            }
            return episodes
        }
    }

    override fun episodeExists(podcastId: Int, episodeTitle: String): Boolean {
        dataSource.connection.use { connection ->
            val statement = connection.prepareStatement("SELECT id FROM episode WHERE podcastid = ? AND title = ?;")
            statement.setInt(1, podcastId)
            statement.setString(2, episodeTitle)
            statement.execute()

            val rs = statement.resultSet
            return rs.next()
        }
    }
}
