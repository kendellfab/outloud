package us.kendellfabrici.outloud.repos

import java.sql.SQLException

import us.kendellfabrici.outloud.models.Podcast

interface PodcastRepo {
    @Throws(SQLException::class)
    fun savePodcast(podcast: Podcast): Int?

    fun listPodcasts(): List<Podcast>
}
