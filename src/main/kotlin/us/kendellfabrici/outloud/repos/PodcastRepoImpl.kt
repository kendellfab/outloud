package us.kendellfabrici.outloud.repos

import com.zaxxer.hikari.HikariDataSource

import java.sql.SQLException
import java.sql.Statement

import us.kendellfabrici.outloud.models.Podcast

class PodcastRepoImpl @Throws(SQLException::class)
constructor(internal var dataSource: HikariDataSource) : PodcastRepo {

    init {

        init()
    }

    @Throws(SQLException::class)
    private fun init() {
        dataSource.connection.use { connection ->
            connection.prepareStatement("CREATE TABLE IF NOT EXISTS podcast(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "title VARCHAR(2048)," +
                    "author VARCHAR(1024)," +
                    "coverurl TEXT," +
                    "feedurl TEXT," +
                    "description TEXT);").execute()
        }
    }

    @Throws(SQLException::class)
    override fun savePodcast(podcast: Podcast): Int? {
        dataSource.connection.use { connection ->
            val statement = connection.prepareStatement("INSERT INTO podcast(title, author, coverurl, feedurl, description) VALUES(?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)
            statement.setString(1, podcast.title)
            statement.setString(2, podcast.author)
            statement.setString(3, podcast.coverURL)
            statement.setString(4, podcast.feedURL)
            statement.setString(5, podcast.description)

            statement.executeUpdate()

            val rs = statement.generatedKeys
            return if (rs.next()) {
                rs.getInt(1)
            } else null
        }
    }

    override fun listPodcasts(): List<Podcast> {
        dataSource.connection.use {
            val statement = it.prepareStatement("SELECT id, title, author, coverurl, feedurl, description FROM podcast ORDER BY title ASC;")
            statement.execute()
            val rs = statement.resultSet

            val podcasts = ArrayList<Podcast>()
            while(rs.next()) {
                val podcast = Podcast()
                podcast.id = rs.getInt(1)
                podcast.title = rs.getString(2)
                podcast.author = rs.getString(3)
                podcast.coverURL = rs.getString(4)
                podcast.feedURL = rs.getString(5)
                podcast.description = rs.getString(6)

                podcasts.add(podcast)
            }
            return podcasts
        }
    }
}
