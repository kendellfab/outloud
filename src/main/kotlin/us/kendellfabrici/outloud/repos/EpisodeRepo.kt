package us.kendellfabrici.outloud.repos

import java.sql.SQLException

import us.kendellfabrici.outloud.models.Episode

interface EpisodeRepo {
    @Throws(SQLException::class)
    fun saveEpisode(episode: Episode)
    fun listEpisodes(podcastId: Int): List<Episode>
    fun episodeExists(podcastId: Int, episodeTitle: String): Boolean
}
