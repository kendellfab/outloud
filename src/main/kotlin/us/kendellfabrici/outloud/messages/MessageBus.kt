package us.kendellfabrici.outloud.messages

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class MessageBus {

    private val publishSubject: PublishSubject<Any> = PublishSubject.create()

    fun passMessage(message: Any) {
        publishSubject.onNext(message)
    }

    fun getEvents(): Observable<Any> {
        return publishSubject
    }

}