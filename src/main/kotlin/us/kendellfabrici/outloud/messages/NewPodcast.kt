package us.kendellfabrici.outloud.messages

import us.kendellfabrici.outloud.models.Podcast

data class NewPodcast(val podcast: Podcast) {
}