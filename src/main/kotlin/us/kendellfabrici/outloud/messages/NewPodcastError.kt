package us.kendellfabrici.outloud.messages

data class NewPodcastError(val message: String) {
}